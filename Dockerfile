FROM debian:bookworm-slim

RUN apt update ; \
    apt-get install -y --no-install-recommends rsyslog ; \
    rm -rf /var/lib/apt/lists/*

CMD [ "/usr/sbin/rsyslogd", "-n", "-iNONE" ]
