# rsyslog image and deployment

This repository provides the means to build a rsyslog
container image and a deployment file to deploy a
rsyslog deployment into a kubernetes cluster.

use `docker build` to build the container image:

```sh
docker build --progress plain -t registry.gitlab.com/uhurutec/repository/rsyslog:"$(cat .next-version)" .
```

use `docker push` to publish the container image:

```sh
docker push registry.gitlab.com/uhurutec/repository/rsyslog:"$(cat .next-version)"
```

The deployment files for kubernetes can be found together
with a kustomization.yaml in the rsyslog directory.
